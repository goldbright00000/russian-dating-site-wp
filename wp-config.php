<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'multil16_wp824');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'el6yyllglmvufcuobqy8lqwd6mtvnhltrkltpxgk6edmpblyg4lwo2bwd2uevuyd');
define('SECURE_AUTH_KEY',  'rp7j2mf35zqut5pgqgvkfr2jp3mdh5qxrshyyvuk5fop80uq67gkstnwz2c154wp');
define('LOGGED_IN_KEY',    'n4rc5qu57loq4evgipophg5jfvx6hj6bovv2vyisywbphqkfqvtndocnysdxydyk');
define('NONCE_KEY',        'gwroiptwnnmpwzd2fxif5ufc4wlwnncflqvirbu3awf4bajx9mywechczpw9oyzg');
define('AUTH_SALT',        'jyrstqeqrbwg3celxnobdfko7g2rwrp15unv6yt5jjq3mv0ioem6gou9vgnrx2zf');
define('SECURE_AUTH_SALT', 'nechlxqoknzw5vwktl2vtsp8adytro2bcbcvttd88zvbjbkhhso2vewnl8efplsz');
define('LOGGED_IN_SALT',   '5f0uxvfn3jinutjltnbfwgxksx4y0msikuzpoppvky5813pneeikqglzjwgxxjv2');
define('NONCE_SALT',       'fi4xrsabw1wuttmy6cqchoquwovtkm06vh8fqkkq3zqfsmdgbic3epeixibnfrx1');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpot_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

# Disables all core updates. Added by SiteGround Autoupdate:
define( 'WP_AUTO_UPDATE_CORE', false );



@include_once('/var/lib/sec/wp-settings.php'); // Added by SiteGround WordPress management system

